/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoiii;

import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author x335875
 */
public class Controlador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Socket skt1 = null;
        Socket skt2 = null;
        Socket skt3 = null;
        Socket cliente = null;
        ServerSocket server = null;
        int porta = 9870;
        
        ArrayList<Socket> servidores = new ArrayList<>();
        
        //conexão
        try{
          server = new ServerSocket(porta);  
        } catch(Exception e){
            System.out.println("Erro ao fazer bind na porta " + porta);
            return;
        }

        try {
            while (true){
            System.out.println("Aguardando cliente...");
            cliente = server.accept();
            
            try {
               skt1 = new Socket("127.0.0.1", 9876);
               servidores.add(0, skt1);
            } catch (Exception e) {
            }
            try {
               skt2 = new Socket("127.0.0.1", 9875);
               servidores.add(1, skt2);
            } catch (Exception e) {
            }
            try {
               skt3 = new Socket("127.0.0.1", 9874);
               servidores.add(skt3);
            } catch (Exception e) {
            }    
 
        // leitura e escrita
            try{
                Scanner scncli = new Scanner(cliente.getInputStream());
                String msg = scncli.nextLine();
                EntendeMsg processa = new EntendeMsg(msg);
                PrintStream printst = null;
                Scanner scn = null;
                if (processa.comando().equals("SELECT")){
                    for (int i = 0; servidores.get(i).isConnected(); i++) {
                       printst =  new PrintStream(servidores.get(0).getOutputStream());
                       printst.println(msg);
                       scn = new Scanner(servidores.get(i).getInputStream());
                       if(scn.hasNext()){
                           String msgrecebida = scn.nextLine();
                           printst =  new PrintStream(cliente.getOutputStream());
                           printst.println(msgrecebida);
                           break;
                       } 
                   }   
               }
                else {
                   if(servidores.size() >=3){
                       String msgrecebida = "";
                   if (servidores.get(0).isConnected()
                           && servidores.get(1).isConnected()
                           && servidores.get(2).isConnected()){
                      printst =  new PrintStream(servidores.get(0).getOutputStream());
                      printst.println(msg);
                      scn = new Scanner(servidores.get(0).getInputStream());
                      if(scn.hasNext()){
                          msgrecebida = scn.nextLine();
                      }
                      printst =  new PrintStream(servidores.get(1).getOutputStream());
                      printst.println(msg);
                      scn = new Scanner(servidores.get(1).getInputStream());
                      if(scn.hasNext()){
                          msgrecebida = scn.nextLine();
                      }
                      printst =  new PrintStream(servidores.get(2).getOutputStream());
                      printst.println(msg);
                      scn = new Scanner(servidores.get(2).getInputStream());
                      if(scn.hasNext()){
                          msgrecebida = scn.nextLine();
                      }
                      printst =  new PrintStream(cliente.getOutputStream());
                      printst.println(msgrecebida);
                    } 
                } else {
                    printst =  new PrintStream(cliente.getOutputStream());
                    printst.println("Impossível executar comando");
                   }
            }
        } catch(Exception e){
            System.out.println("Erro ao ler/escrever: " + e.getMessage());
        }
        servidores.removeAll(servidores);
        try{
            cliente.close();
            
        } catch(Exception e){
            System.out.println("Erro ao desconectar");
               }
                }
    
        } catch (Exception e) {
        }
    }
    }