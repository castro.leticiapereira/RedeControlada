/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoiii;

import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author x335875
 */
public class Cliente {
    public static void main(String[] args) {
        Socket skt = null;
        try{
          skt = new Socket("127.0.0.1", 9870);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try{
            PrintStream printst = new PrintStream(skt.getOutputStream());
            printst.println("delete: Ana");
            Scanner scn = new Scanner(skt.getInputStream());
            String resposta = scn.nextLine();
            System.out.println(resposta);

        } catch(Exception e){
            System.out.println("Erro ao ler/escrever: " + e.getMessage());
        }
        try {
            skt.close();
        } catch (Exception e) {
            System.out.println("Erro ao encerrar a conexão");
        }
    }
}